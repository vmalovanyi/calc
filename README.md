####System Requirements:
 - Java 8
 
##Gradle commands
##### Building and testing the application
```
 gradle clean build 
```
Jar file will be located build/libs/calc.jar
```
 gradle clean test
```
 
##Runing application

```
java -jar calc.jar
```
After this application will ask to provide file name to load. Its expected that file in the same folder as jar of application.
######example:
```
>fileName.txt
```

Expected values - BigDecimals. Result will be displayed with scale 2 and ceiling rounding. 