package com.ngsoft.calc;

import com.ngsoft.calc.core.model.FileLoaderProxy;
import com.ngsoft.calc.core.util.MathHandler;
import com.ngsoft.calc.core.util.StringParser;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.LinkedList;

public class GeneralTest {

    private FileLoaderProxy loader;
    private MathHandler handler;
    private StringParser parser;

    @Before
    public void prepare(){
        loader = new FileLoaderProxy();
        parser = new StringParser();
    }

    @Test
    public void multiplicationTest(){
        loader.loadFile("src/test/resources/instructions/multiplication.instruction");
        BigDecimal result = processInstruction(loader.getInstructions());
        Assert.assertEquals(result.intValue(), 45);
    }

    @Test
    public void sumTest(){
        loader.loadFile("src/test/resources/instructions/sum.instruction");
        BigDecimal result = processInstruction(loader.getInstructions());
        Assert.assertEquals(result.intValue(), 9);
    }

    @Test
    public void negativeResultTest(){
        loader.loadFile("src/test/resources/instructions/negative_result.instruction");
        BigDecimal result = processInstruction(loader.getInstructions());
        Assert.assertEquals(result.intValue(), -2);
    }

    @Test
    public void negativeNumbersTest(){
        loader.loadFile("src/test/resources/instructions/negative_numbers.instruction");
        BigDecimal result = processInstruction(loader.getInstructions());
        Assert.assertEquals(result.intValue(), 7);
    }

    @Test
    public void subtractNumbersTest(){
        loader.loadFile("src/test/resources/instructions/subtract.instruction");
        BigDecimal result = processInstruction(loader.getInstructions());
        Assert.assertEquals(result.intValue(), 4);
    }

    @Test
    public void noInstructionTest(){
        loader.loadFile("src/test/resources/instructions/no.instruction");
        BigDecimal result = processInstruction(loader.getInstructions());
        Assert.assertEquals(result.intValue(), 1);
    }

    @Test
    public void multiplyZeroTest(){
        loader.loadFile("src/test/resources/instructions/multiply_0.instruction");
        BigDecimal result = processInstruction(loader.getInstructions());
        Assert.assertEquals(result.intValue(), 0);
    }

    @Test(expected = ArithmeticException.class)
    public void divideZeroTest(){
        loader.loadFile("src/test/resources/instructions/divide_0.instruction");
        BigDecimal result = processInstruction(loader.getInstructions());
    }

    private BigDecimal processInstruction(LinkedList<String> instructions) {

        handler = new MathHandler(parser.parse(instructions.pollFirst()).getValue());

        while (!instructions.isEmpty()){
            handler.applyInstruction(parser.parse(instructions.pollLast()));
        }

        return handler.getValue();
    }
}
