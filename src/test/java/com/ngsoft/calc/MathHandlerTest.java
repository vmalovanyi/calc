package com.ngsoft.calc;

import com.ngsoft.calc.core.config.ActionList;
import com.ngsoft.calc.core.model.dto.Instruction;
import com.ngsoft.calc.core.util.MathHandler;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;

public class MathHandlerTest {

    private MathHandler handler;

    @Before
    public void prepare(){
        handler = new MathHandler(new BigDecimal(4));
    }

    @Test
    public void sumTest(){
        handler.applyInstruction(new Instruction(ActionList.ADD, new BigDecimal(8)));
        Assert.assertEquals(handler.getValue().intValue(), 12);
    }

    @Test
    public void divisionTest(){
        handler.applyInstruction(new Instruction(ActionList.DIVIDE, new BigDecimal(2)));
        Assert.assertEquals(handler.getValue().intValue(), 2);
    }

    @Test (expected = ArithmeticException.class)
    public void divisionByZeroTest(){
        handler.applyInstruction(new Instruction(ActionList.DIVIDE, new BigDecimal(0)));
    }

    @Test
    public void subtractTest(){
        handler.applyInstruction(new Instruction(ActionList.SUBTRACT, new BigDecimal(1)));
        Assert.assertEquals(handler.getValue().intValue(), 3);
    }

    @Test
    public void multiplicationTest(){
        handler.applyInstruction(new Instruction(ActionList.MULTIPLY, new BigDecimal(4)));
        Assert.assertEquals(handler.getValue().intValue(), 16);
    }
}
