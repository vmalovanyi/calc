package com.ngsoft.calc;

import com.ngsoft.calc.core.config.ActionList;
import com.ngsoft.calc.core.model.dto.Instruction;
import com.ngsoft.calc.core.util.StringParser;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

public class ParserTest {

    private StringParser parser;

    @Before
    public void prepare(){
        parser = new StringParser();
    }

    @Test
    public void parseStringWithNumbers(){
        Instruction inst1 = parser.parse("add 0.45");
        Assert.assertNotNull(inst1);
        Assert.assertEquals(inst1.getAction(), ActionList.ADD);
        Assert.assertEquals(inst1.getValue().toString(), "0.45");
        Assert.assertEquals(inst1.getValue().doubleValue(), 0.45, 0);
    }

    @Test
    public void parseStringWithNegativeValues(){
        Instruction inst1 = parser.parse("add -0.45");
        Assert.assertNotNull(inst1);
        Assert.assertEquals(inst1.getAction(), ActionList.ADD);
        Assert.assertEquals(inst1.getValue().toString(), "-0.45");
        Assert.assertEquals(inst1.getValue().doubleValue(), -0.45, 0);

        Instruction inst2 = parser.parse("divide -1001");
        Assert.assertNotNull(inst2);
        Assert.assertEquals(inst2.getAction(), ActionList.DIVIDE);
        Assert.assertEquals(inst2.getValue().toString(), "-1001");
        Assert.assertEquals(inst2.getValue().intValue(), -1001);
    }

    @Test
    public void parseStringWithIntegers(){
        Instruction inst1 = parser.parse("divide 1001");
        Assert.assertNotNull(inst1);
        Assert.assertEquals(inst1.getAction(), ActionList.DIVIDE);
        Assert.assertEquals(inst1.getValue().toString(), "1001");
        Assert.assertEquals(inst1.getValue().intValue(), 1001);
    }

    @Test
    public void parseStringWithUnsupportedValue(){
        Instruction inst1 = parser.parse("add 2^31-1");
        Instruction inst2 = parser.parse("add 123e");
        Instruction inst3 = parser.parse("add 1,23");
        Assert.assertNull(inst1);
        Assert.assertNull(inst2);
        Assert.assertNull(inst3);
    }


}
