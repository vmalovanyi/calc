package com.ngsoft.calc;

import com.ngsoft.calc.core.model.FileLoaderProxy;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class FileLoaderProxyTest {

    private FileLoaderProxy loader;

    @Before
    public void prepare(){
        loader = new FileLoaderProxy();
    }

    @Test
    public void fileLoadedSuccessfullyTest(){
        loader.loadFile("src/test/resources/instructions/negative_numbers.instruction");
        Assert.assertTrue(loader.isFileLoaded());
    }

    @Test
    public void fileLoadingErrorTest(){
        loader.loadFile("src/test/resources/instructions/notExisting.instruction");
        Assert.assertFalse(loader.isFileLoaded());
    }
}
