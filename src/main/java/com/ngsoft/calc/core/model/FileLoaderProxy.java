package com.ngsoft.calc.core.model;

import com.ngsoft.calc.core.config.UserInfoNotification;
import org.puremvc.java.patterns.proxy.Proxy;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.LinkedList;
import java.util.stream.Stream;

public class FileLoaderProxy extends Proxy {

    private LinkedList<String> instructions;
    private boolean isFileLoaded;

    public static final String NAME = "FileLoaderProxy";

    public FileLoaderProxy() {
        super(NAME);
    }

    public void loadFile(String pathAndName){

        isFileLoaded = false;

        Path path = Paths.get(System.getProperty("user.dir") + File.separator + pathAndName);

        sendNotification(UserInfoNotification.SHOW_ON_SCREEN, "Trying to access file " + path.toString());

        try (Stream<String> lines = Files.lines(path)) {

            instructions = new LinkedList<>();

            lines.forEach(s -> instructions.push(s));

            isFileLoaded = true;

        } catch (IOException ex) {

            isFileLoaded = false;
        }
    }

    public boolean isFileLoaded() {
        return isFileLoaded;
    }

    public LinkedList<String> getInstructions (){
        return instructions;
    }
}
