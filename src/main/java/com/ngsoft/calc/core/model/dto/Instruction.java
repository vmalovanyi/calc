package com.ngsoft.calc.core.model.dto;

import java.math.BigDecimal;

public class Instruction {
    private String action;
    private BigDecimal value;

    public Instruction(String action, BigDecimal value) {
        this.action = action;
        this.value = value;
    }

    public String getAction() {
        return action;
    }

    public BigDecimal getValue() {
        return value;
    }
}
