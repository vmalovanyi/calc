package com.ngsoft.calc.core.config;

import java.util.Arrays;
import java.util.List;

public class ActionList {
    public static final String ADD = "ADD";
    public static final String SUBTRACT = "SUBTRACT";
    public static final String DIVIDE = "DIVIDE";
    public static final String MULTIPLY = "MULTIPLY";
    public static final String APPLY = "APPLY";

    public static List<String> ALL = Arrays.asList(ADD, SUBTRACT, DIVIDE, MULTIPLY, APPLY);
}
