package com.ngsoft.calc.core.config;

public class OutputInfo {
    public static final String ON_USER_INTERACTION_READY = "Please, type name of instruction to load:";
    public static final String ON_FILE_LOADING_ERROR = "Sorry, file is missing...";
    public static final String ON_INSTRUCTION_PARSING_ERROR = "Sorry, instruction is corrupted...";
    public static final String ON_PROHIBITED_INSTRUCTION = "Sorry, following instruction is not allowed - ";
    public static final String ON_MISSING_INSTRUCTIONS = "File does not contain any instructions to compute!" ;
    public static final String ON_MISSING_APPLY_INSTRUCTION = "File does not contain instruction to apply!" ;
}
