package com.ngsoft.calc.core.config;

public class GeneralNotifications {
    public static final String STARTUP = "STARTUP";
    public static final String READY_FOR_USER_INTERACTION = "READY_FOR_USER_INTERACTION";
    public static final String PROCESS_USER_INPUT = "PROCESS_USER_INPUT";
    public static final String CHANGE_CONSOLE_STATUS = "CHANGE_CONSOLE_STATUS";
    public static final String PROCESS_LOADED_FILE = "PROCESS_LOADED_FILE";
}
