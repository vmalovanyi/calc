package com.ngsoft.calc.core.controller;

import com.ngsoft.calc.core.config.ActionList;
import com.ngsoft.calc.core.config.GeneralNotifications;
import com.ngsoft.calc.core.config.OutputInfo;
import com.ngsoft.calc.core.config.UserInfoNotification;
import com.ngsoft.calc.core.model.FileLoaderProxy;
import com.ngsoft.calc.core.model.dto.Instruction;
import com.ngsoft.calc.core.util.MathHandler;
import com.ngsoft.calc.core.util.StringParser;
import org.puremvc.java.interfaces.INotification;
import org.puremvc.java.patterns.command.SimpleCommand;

import java.math.BigDecimal;
import java.util.LinkedList;

public class ProcessLoadedFileCommand extends SimpleCommand {

    @Override
    public void execute(INotification notification) {

        FileLoaderProxy fileLoader = (FileLoaderProxy) facade.retrieveProxy(FileLoaderProxy.NAME);
        StringParser parser = new StringParser();

        LinkedList<String> instructions = fileLoader.getInstructions();

        if(instructions.isEmpty()){
            sendNotification(UserInfoNotification.SHOW_ON_SCREEN, OutputInfo.ON_MISSING_INSTRUCTIONS);
            enableUserInteraction();
            return;
        }

        Instruction instructionApplyTo = parser.parse(instructions.pollFirst());
        if (!instructionApplyTo.getAction().equals(ActionList.APPLY)){
            sendNotification(UserInfoNotification.SHOW_ON_SCREEN, OutputInfo.ON_MISSING_APPLY_INSTRUCTION);
            enableUserInteraction();
            return;
        }

        MathHandler mathHandler = new MathHandler(instructionApplyTo.getValue());

        Instruction instruction;
        while (!instructions.isEmpty()){
            instruction = parser.parse(instructions.pollLast());
            if (instruction == null){
                sendNotification(UserInfoNotification.SHOW_ON_SCREEN, OutputInfo.ON_INSTRUCTION_PARSING_ERROR);
                enableUserInteraction();
                return;
            }

            try {
                mathHandler.applyInstruction(instruction);
            } catch (ArithmeticException exc){
                sendNotification(UserInfoNotification.SHOW_ON_SCREEN, OutputInfo.ON_PROHIBITED_INSTRUCTION + instruction.getAction() + " " + instruction.getValue());
                enableUserInteraction();
                return;
            }
        }

        sendNotification(UserInfoNotification.SHOW_ON_SCREEN, "Result is: " + mathHandler.getValue());
        enableUserInteraction();
    }

    private void enableUserInteraction() {
        sendNotification(UserInfoNotification.SHOW_ON_SCREEN, OutputInfo.ON_USER_INTERACTION_READY);
        sendNotification(GeneralNotifications.CHANGE_CONSOLE_STATUS, true);
    }
}
