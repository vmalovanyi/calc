package com.ngsoft.calc.core.controller;

import com.ngsoft.calc.core.config.GeneralNotifications;
import com.ngsoft.calc.core.config.OutputInfo;
import com.ngsoft.calc.core.config.UserInfoNotification;
import com.ngsoft.calc.core.model.FileLoaderProxy;
import com.ngsoft.calc.core.view.mediator.RootMediator;
import org.puremvc.java.interfaces.INotification;
import org.puremvc.java.patterns.command.SimpleCommand;

public class StartupCommand extends SimpleCommand {

    @Override
    public void execute(INotification notification) {

        facade.registerCommand(GeneralNotifications.READY_FOR_USER_INTERACTION, new UserInteractionCommand());
        facade.registerCommand(GeneralNotifications.PROCESS_USER_INPUT, new ProcessUserInputCommand());
        facade.registerCommand(GeneralNotifications.PROCESS_LOADED_FILE, new ProcessLoadedFileCommand());

        facade.registerProxy(new FileLoaderProxy());
        facade.registerMediator(new RootMediator());
    }
}
