package com.ngsoft.calc.core.controller;

import com.ngsoft.calc.core.config.GeneralNotifications;
import com.ngsoft.calc.core.config.OutputInfo;
import com.ngsoft.calc.core.config.UserInfoNotification;
import org.puremvc.java.interfaces.INotification;
import org.puremvc.java.patterns.command.SimpleCommand;

public class UserInteractionCommand extends SimpleCommand {

    @Override
    public void execute(INotification notification) {

        sendNotification(UserInfoNotification.SHOW_ON_SCREEN, OutputInfo.ON_USER_INTERACTION_READY);
        sendNotification(GeneralNotifications.CHANGE_CONSOLE_STATUS, true);
    }
}
