package com.ngsoft.calc.core.controller;

import com.ngsoft.calc.core.config.GeneralNotifications;
import com.ngsoft.calc.core.config.OutputInfo;
import com.ngsoft.calc.core.config.UserInfoNotification;
import com.ngsoft.calc.core.model.FileLoaderProxy;
import org.puremvc.java.interfaces.INotification;
import org.puremvc.java.patterns.command.SimpleCommand;

public class ProcessUserInputCommand extends SimpleCommand {
    @Override
    public void execute(INotification notification) {
        String filePath = (String)notification.getBody();

        sendNotification(GeneralNotifications.CHANGE_CONSOLE_STATUS, false);

        FileLoaderProxy fileLoader = (FileLoaderProxy) facade.retrieveProxy(FileLoaderProxy.NAME);
        fileLoader.loadFile(filePath);
        if (fileLoader.isFileLoaded()){
            sendNotification(GeneralNotifications.PROCESS_LOADED_FILE);
        } else {
            sendNotification(UserInfoNotification.SHOW_ON_SCREEN, OutputInfo.ON_FILE_LOADING_ERROR);
            sendNotification(UserInfoNotification.SHOW_ON_SCREEN, OutputInfo.ON_USER_INTERACTION_READY);
            sendNotification(GeneralNotifications.CHANGE_CONSOLE_STATUS, true);
        }
    }
}
