package com.ngsoft.calc.core.util;

import com.ngsoft.calc.core.config.ActionList;
import com.ngsoft.calc.core.model.dto.Instruction;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class MathHandler {

    private BigDecimal value;

    public MathHandler(BigDecimal value) {

        this.value = value;
    }

    public void applyInstruction(Instruction instruction) throws ArithmeticException{

        switch (instruction.getAction()){
            case ActionList.ADD:
                value = value.add(instruction.getValue());
                break;
            case ActionList.SUBTRACT:
                value = value.subtract(instruction.getValue());
                break;
            case ActionList.MULTIPLY:
                value = value.multiply(instruction.getValue());
                break;
            case ActionList.DIVIDE:
                value = value.divide(instruction.getValue(), 2, RoundingMode.CEILING);
                break;
        }
    }

    public BigDecimal getValue() {
        return value.setScale(2, RoundingMode.CEILING);
    }


}
