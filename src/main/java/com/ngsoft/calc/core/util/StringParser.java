package com.ngsoft.calc.core.util;

import com.ngsoft.calc.core.config.ActionList;
import com.ngsoft.calc.core.model.dto.Instruction;

import java.math.BigDecimal;

public class StringParser {

    public Instruction parse(String line) {

        if (line == null || line.length() == 0 || !line.contains(" ")){
            return null;
        }

        String[] arr = line.split(" ");

        if (arr.length > 2){
            return null;
        }

        String action = typoChecker(arr[0].toUpperCase());

        if (action == null || !ActionList.ALL.contains(action)){
            return null;
        }

        BigDecimal value;

        try {
            value = new BigDecimal(arr[1]);
        } catch (NumberFormatException nfe){
            return null;
        }


        return new Instruction(action, value);
    }

    private String typoChecker(String action) {



        return action;
    }
}
