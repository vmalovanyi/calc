package com.ngsoft.calc.core.view.mediator;

import com.ngsoft.calc.core.config.GeneralNotifications;
import com.ngsoft.calc.core.config.UserInfoNotification;
import org.puremvc.java.interfaces.INotification;
import org.puremvc.java.patterns.mediator.Mediator;

import java.util.Scanner;

public class RootMediator extends Mediator {

    public static final String NAME = "RootMediator";

    private Scanner scanner;
    private boolean isReadyForInteraction;

    public RootMediator() {
        super(NAME, null);
    }

    @Override
    public void onRegister() {
        super.onRegister();
        isReadyForInteraction = false;
        scanner = new Scanner(System.in);

        sendNotification(GeneralNotifications.READY_FOR_USER_INTERACTION);
    }

    private void startListenUserInput() {
        while (isReadyForInteraction){
            String fileName = scanner.nextLine();
            handleFileNameInput(fileName);
        }
    }

    private void handleFileNameInput(String fileName) {
        sendNotification(GeneralNotifications.PROCESS_USER_INPUT, fileName);
    }

    @Override
    public void handleNotification(INotification notification) {
        super.handleNotification(notification);
        switch (notification.getName()){
            case UserInfoNotification.SHOW_ON_SCREEN:
                writeTextToConsole((String)notification.getBody());
                break;
            case GeneralNotifications.CHANGE_CONSOLE_STATUS:
                isReadyForInteraction =  (Boolean) notification.getBody();
                 if (isReadyForInteraction){
                     startListenUserInput();
                 }
                break;
        }
    }

    private void writeTextToConsole(String value) {
        System.out.println(value);
    }

    @Override
    public String[] listNotificationInterests() {
        return new String[]{UserInfoNotification.SHOW_ON_SCREEN,
                            GeneralNotifications.CHANGE_CONSOLE_STATUS};
    }
}
