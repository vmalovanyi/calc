package com.ngsoft.calc;

import com.ngsoft.calc.core.ApplicationFacade;
import com.ngsoft.calc.core.controller.StartupCommand;

public class Main {

    public static void main(String[] args) {

        ApplicationFacade.getInstance().startup(new StartupCommand());
    }
}
